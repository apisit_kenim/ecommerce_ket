
<?php
	session_start();
?>

<div class="site-navbar bg-white py-2">
  <div class="container">
	<div class="float-right">
		<div id="google_translate_element" class="topbar-language"></div>
					<script type="text/javascript">
					function googleTranslateElementInit() {
						new google.translate.TranslateElement({
							pageLanguage: 'th',
							includedLanguages: 'en,th'
						}, 'google_translate_element');
					}
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					<style>.goog-logo-link { display: none; } .goog-te-gadget { font-size: 0px; }</style>
				</div>
			</div>
<br>
  <!-- <div class="search-wrap">
    <div class="container">
      <a href="#" class="search-close js-search-close"><span class="icon-close2"></span></a>
      <form action="#" method="post">
        <input type="text" class="form-control" placeholder="Search keyword and hit enter...">
      </form>
    </div>
  </div> -->

  <div class="container">
    <div class="d-flex align-items-center justify-content-between">
      <div class="logo">
        <div class="site-logo">
          <!-- <a href="index.php" class="js-logo-clone">I LOVE MY SKIN</a> -->
        </div>
      </div>
      <div class="main-nav d-none d-lg-block">
        <nav class="site-navigation text-right text-md-center" role="navigation">
          <ul class="site-menu js-clone-nav d-none d-lg-block">

            <li>
              <a href="index.php">หน้าหลัก</a>
            </li>

            <!-- <li><a href="shop.html">สินค้า</a></li>
            <li><a href="#">สมัครสมาชิก</a></li>
            <li><a href="#">เว็บบอร์ด</a></li>
            <li><a href="#">เข้าสู่ระบบ</a></li>
            <li><a href="contact.html">ติดต่อเรา</a></li>
						 -->

						 <li>
							  <a href="product.php">สินค้า</a>
							</li>

							<?php if ($_SESSION["isLogin"] == ''){ ?>
							<li>
							  <a href="register.php">สมัครสมาชิก</a>
							</li>
							<?php } ?>

							<?php if ($_SESSION["isLogin"] == '1'){ ?>
							<li>
							  <a href="info.php?id=<?php echo $_SESSION["memberid"];?>">ข้อมูลส่วนตัว</a>
							</li>
							<li>
							  <a href="history.php">ประวัติการสั่งซื้อ</a>
							</li>
							<li>
							  <a href="transferInfos.php">การชำระเงิน</a>
							</li>
							<li>
							  <a href="transfer.php">แจ้งชำระเงิน</a>
							</li>

							<?php }?>

							<li>
							  <a href="contact.php">ติดต่อเรา</a>
							</li>

							<li>
							  <a href="Webboard.php">เว็บบอร์ด</a>
							</li>

							<?php if ($_SESSION["isLogin"] == ''){?>
							<li>
							  <a href="login.php">เข้าสู่ระบบ</a>
							</li>
							<?php } ?>

							<?php if ($_SESSION["isLogin"] == '1'){?>
							  <li>
							    <a href="logout.php">ออกจากระบบ</a>
							  </li>
							 <?php } ?>

          </ul>
        </nav>
      </div>
      <div class="icons">
        <!-- <a href="#" class="icons-btn d-inline-block js-search-open"><span class="icon-search"></span></a> -->
        <!-- <a href="#" class="icons-btn d-inline-block"><span class="icon-heart-o"></span></a> -->
        <a href="cart.php" class="icons-btn d-inline-block bag">
          <span class="icon-shopping-bag"></span>
          <!-- <span class="number">2</span> -->
        </a>
        <a href="#" class="site-menu-toggle js-menu-toggle ml-3 d-inline-block d-lg-none"><span class="icon-menu"></span></a>
      </div>
    </div>
  </div>
</div>
