<?php
session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

?>

<div class="container">
  <div class="row">
    <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
      <!-- <h3 class="footer-heading mb-4">Promo</h3> -->
      <!-- <a href="#" class="block-6"> -->
        <div class="logo">
          <div class="site-logo">
            <a href="index.php" class="js-logo-clone">ธาดาเซรามิก</a>
          </div>
        </div>
        <!-- <img src="images/about_1.jpg" alt="Image placeholder" class="img-fluid rounded mb-4"> -->
        <!-- <h3 class="font-weight-light  mb-0">Finding Your Perfect Shirts This Summer</h3> -->
        <!-- <p>Promo from  July 15 &mdash; 25, 2019</p> -->
      <!-- </a> -->
    </div>
    <div class="col-lg-5 ml-auto mb-5 mb-lg-0">
      <div class="row">
        <div class="col-md-12">
          <h3 class="footer-heading mb-4">ลิงค์ต่าง ๆ </h3>
        </div>
        <div class="col-md-6 col-lg-4">
          <ul class="list-unstyled">
            <li><a href="http://track.thailandpost.co.th/tracking/default.aspx" target="_blank">ไปรษณีย์</a></li>
            <li><a href="https://th.kerryexpress.com/en/track/" target="_blank">Kerry Express</a></li>
            <!-- <li><a href="#">Shopping cart</a></li> -->
            <!-- <li><a href="#">Store builder</a></li> -->
          </ul>
        </div>
        <div class="col-md-6 col-lg-4">
          <ul class="list-unstyled">
            <li><a href="contact.php">ติดต่อเรา</a></li>
            <li><a href="Webboard.php">เว็บบอร์ด</a></li>
            <!-- <li><a href="#">Website development</a></li> -->
          </ul>
        </div>
        <div class="col-md-6 col-lg-4">
          <ul class="list-unstyled">
            <li><a href="admin/pages/Login/admin_login.php">สำหรับเจ้าของร้าน</a></li>
            <!-- <li><a href="#">Hardware</a></li> -->
            <!-- <li><a href="#">Software</a></li> -->
          </ul>
        </div>
      </div>
    </div>

    <div class="col-md-6 col-lg-3">
      <div class="block-5 mb-5">
        <h3 class="footer-heading mb-4">ติดต่อเรา</h3>
        <ul class="list-unstyled">
          <li class="address">288 ม.9 ต.ทุ่งฝาย อ.เมือง จ.ลำปาง 52000</li>
          <li class="phone"><a href="tel://0912311111">062-297-7421</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-12 col-lg-12">
        <center>
          <h3 class="footer-heading mb-4">จำนวนผู้เข้าชม : <?php echo $_SESSION["count"];?></h3>
        </center> 
      </div>
    </div>
  <!-- <div class="row pt-5 mt-5 text-center">
    <div class="col-md-12">
      <p>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      <!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | I Love My Skin <i class="icon-heart" aria-hidden="true"></i> </a> -->
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      <!-- </p>
    </div>

  </div> -->
</div>
