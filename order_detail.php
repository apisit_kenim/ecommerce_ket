<?php
session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$tbl_order = $conn->select('orders', array('orderid' => getIsset('id')), true);

if ($tbl_order != null) {
  $refid = $tbl_order["refid"];
  $id_cus = $tbl_order["id_cus"];
  $data = $tbl_order["data"];
  $paystatus = $tbl_order["paystatus"];
  $statusnow = $tbl_order["statusnow"];
  $sent_rate = $tbl_order["sent_rate"];
  $transport = $tbl_order["transport"];

  $sqlDetail = "select * from orderdetails where orderid = $id";
  $tbl_orderDetail = $conn->queryRaw($sqlDetail);
}

if ($cmd == 'save_product') {
  $value = array(
    "total" => getIsset('_total')
  );

  $chk_isnew = $conn->select('member', array('phone' => getIsset('_phone')), true);
  
  $USERNAME = 'suriyapanoi';
  $PASSWORD = 'ebcd14';
  $FROM = '0000';
  $TO = $_SESSION["memberPhone"];
  $MESSAGE = 'หมายเลขคำสั่งซื้อ : '.$tbl_order["refid"].' กรุณาชำระเงินภายใน 2 วันค่ะ';

  // file_get_contents('http://www.thsms.com/api/rest?method=send&username='.$USERNAME.'&password='.$PASSWORD.'&from='.$FROM.'&to='.$TO.'&message='.$MESSAGE);
 
  // $url = 'http://www.thsms.com/api/rest?method=send&username='.$USERNAME.'&password='.$PASSWORD.'&from='.$FROM.'&to='.$TO.'&message='.$MESSAGE.'';

  // $url = 'http://www.thsms.com/api/rest?method=send&username='.$USERNAME.'&password='.$PASSWORD.'&from='.$FROM.'&to='.$TO.'&message='.$MESSAGE.'';
  // getUrlContent($url);
  // $var_dump[$url];

  // echo $url;

  $params['method']   = 'send';
  $params['username'] = $USERNAME;
  $params['password'] = $PASSWORD;

  $params['from']     = $FROM;
  $params['to']       = $TO;
  $params['message']  = $MESSAGE;

  if (is_null( $params['to']) || is_null( $params['message']))
  {
      return FALSE;
  }

  $result = curl( $params);

//   $xml = @simplexml_load_string( $result);
//   if (!is_object($xml))
//   {
//       return array( FALSE, 'Respond error');
//   } else {
//       if ($xml->send->status == 'success')
//       {
//           return array( TRUE, $xml->send->uuid);
//       } else {
//           return array( FALSE, $xml->send->message);
//       }
//   }


//   

  if ($conn->update("orders", $value, array("orderid" => $id))) {
    redirectTo("history.php");
  } else {
    alertMassage("ไม่สามารถบันทึกข้อมูลได้");
  }

}

function curl( $params=array())
{
  $api_url   = 'http://www.thsms.com/api/rest';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $api_url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $params));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $response  = curl_exec($ch);
  $lastError = curl_error($ch);
  $lastReq = curl_getinfo($ch);
  curl_close($ch);

  return $response;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>
    <!-- action="history.php" -->
    <form method="post">
      <div class="site-section">
        <div class="container">
          <div class="col-md-12">
            <label class="text-black h4" for="coupon">เลขที่ใบสั่งซื้อ : <?php echo $refid; ?></label>
          </div>
          <div class="row mb-5">
            <div class="col-md-12">
              <div class="site-blocks-table">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th class="product-thumbnail">รูป</th>
                      <th class="product-name">สินค้า</th>
                      <th class="product-price">ราคา</th>
                      <th class="product-quantity">จำนวน</th>
                      <th class="product-total">รวม</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php

                    $Total = 0;
                    $SumTotal = 0;

                    $index = 0;
                    foreach ($tbl_orderDetail as $row) {
                      $index++;

                      $strProductID = $row["productid"];
                      $objResult = $conn->select('products', array('productid' => $strProductID), true);

                      $Total =  $row["qty"] * $objResult["productsprice"];
                      $SumTotal = $SumTotal + $Total;
                      $numberCount = $row["qty"] + $numberCount;
                    ?>

                      <tr>
                        <td class="product-thumbnail">
                          <img src="images/<?php echo $objResult["productsphoto"]; ?>" alt="Image" class="img-fluid">
                        </td>
                        <td class="product-name">
                          <h2 class="h5 text-black"><?php echo $objResult["productsname"]; ?></h2>
                        </td>
                        <td>฿<?php echo $row["price"]; ?></td>
                        <td> <?php echo $row["qty"]; ?></td>
                        <td>฿<?php echo $row["total"]; ?></td>
                      </tr>

                    <?php
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="row mb-5">
                <div class="col-md-6 mb-3 mb-md-0">
                  <!-- <button class="btn btn-primary btn-sm btn-block" >คำนวณราคาสินค้าใหม่</button> -->
                </div>
                <div class="col-md-6">
                  <!-- <button class="btn btn-outline-primary btn-sm btn-block" name="__cmd" value="product">เลือกสินค้าเพิ่ม</button> -->
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <!-- <label class="text-black h4" for="coupon">Coupon</label> -->
                  <!-- <p>Enter your coupon code if you have one.</p> -->
                </div>
                <div class="col-md-8 mb-3 mb-md-0">
                  <!-- <input type="text"  autocomplete="off"  class="form-control py-3" id="coupon" placeholder="Coupon Code"> -->
                </div>
                <div class="col-md-4">
                  <!-- <button class="btn btn-primary btn-sm px-4">Apply Coupon</button> -->
                </div>
              </div>
            </div>
            <div class="col-md-6 pl-5">
              <div class="row justify-content-end">
                <div class="col-md-7">
                  <div class="row">
                    <div class="col-md-12 text-right border-bottom mb-5">
                      <h3 class="text-black h4 text-uppercase">ยอดรวม</h3>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-md-6">
                      <span class="text-black">ค่าขนส่ง</span>
                    </div>
                    <div class="col-md-6 text-right">
                      <?php if ($transport == 'POST') {

                        if ($numberCount > 3) {
                          $transportCount = ($numberCount - 3) * 10;
                        }

                        $transfer = 30;
                        $transfer = $transfer + $transportCount;
                      } else if ($transport == 'EMS') {

                        if ($numberCount > 3) {
                          $transportCount = ($numberCount - 3) * 10;
                        }

                        $transfer = 60;
                        $transfer = $transfer + $transportCount;
                      } else {

                        if ($numberCount > 3) {
                          $transportCount = ($numberCount - 3) * 10;
                        }

                        $transfer = 80;
                        $transfer = $transfer + $transportCount;
                      } ?>

                      <strong class="text-black"><?php echo number_format($transfer, 2); ?></strong>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <div class="col-md-6">
                      <span class="text-black">รวมทั้งหมด</span>
                    </div>
                    <div class="col-md-6 text-right">
                      <?php
                      $Total = $SumTotal + $transfer; ?>
                      <input type="hidden" name="_total" value="<?php echo $Total ?>"></input>
                      <strong class="text-black"><?php echo number_format($Total, 2); ?></strong>
                    </div>
                  </div>
                  <!-- <div class="row mb-5">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">$230.00</strong>
                  </div>
                </div> -->

                  <div class="row">
                    <div class="col-md-12">
                      <!-- <button class="btn btn-primary btn-lg btn-block" name="__cmd" value="save_product">สั่งซื้อสินค้า</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <br><br>

          <div class="row">
            <div class="col-md-12 mb-5 mb-md-0">
              <h2 class="h3 mb-3 text-black">ข้อมูลการจัดส่งสินค้า</h2>
              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">ชื่อ - สกุล : <?php echo $_SESSION["memberName"]; ?></label>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">ที่อยู่ : <?php echo $_SESSION["memberAddress"]; ?></label>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">รหัสไปรษณีย์ : <?php echo $_SESSION["memberZipcode"]; ?></label>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">เบอร์โทร : <?php echo $_SESSION["memberPhone"]; ?></label>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-black btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="__cmd" value="save_product">ตกลง</button>
                </div>
              </div>
            </div>

          </div>


        </div>
      </div>
    </form>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  <script type="text/javascript">

  </script>
</body>

</html>