<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

$date = date("Y-m-d");
$time = date("h:i");

$refid = getIsset("_refid");

$memberid = $_SESSION["memberid"];

$sql1 = "SELECT *  from orders WHERE id_cus = $memberid AND statusnow = 'สั่งซื้อสินค้า'";
$select_all1 = $conn->queryRaw($sql1);
$total1 = sizeof($select_all1);

if($_REQUEST[btnsave] === 'บันทึก')	{
	$statusname = 'สั่งซื้อสินค้า';
	$chk_refid= $conn->select('orders', array('refid' => $refid,'statusnow' => $statusname), true);

	if ($chk_refid == null){
		$chk_refid= $conn->select('orders', array('refid' => $refid,'statusnow' => 'ตรวจสอบยอดชำระใหม่'), true);
	}


 if($chk_refid == null){
	 alertMassage("ไม่พบเลขที่ใบสั่งซื้อ กรุณาตรวจสอบเลขที่สั่งซื้อ");
 }else{

	 if ($_FILES['_slip']['name'] != '') {
	     $path = $_FILES['_slip']['name'];
	     $ext = pathinfo($path, PATHINFO_EXTENSION);
	     $new_image_name = 'img_'.uniqid().".".$ext;
	     $image_path = "images/";
	     $upload_path = $image_path.$new_image_name;

	     //upload to database
	      $isUpload = move_uploaded_file($_FILES['_slip']['tmp_name'], $upload_path);

			}

      if ($isUpload==true){
  			$value = array(
  					"name"=>$refid
  					 ,"datepay"=>getIsset('_paydate')
  					 ,"timepay"=>getIsset('_paytime')
  					 ,"price"=>getIsset('_amount')
  					 ,"slip"=>$new_image_name
  			);
  				 if($conn->create("payment",$value)){
  					 	updateStatusOrder($refid,"รอยืนยันการชำระเงิน");
  						alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','history.php');
  				 }else{
  						alertMassage("ไม่สามารถบันทึกข้อมูลได้");
  				 }
         }
         else{
	          alertMassage("ไม่สามารถอัพโหลดสลิปได้");
    }
  }
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="h3 mb-12 text-black">แจ้งชำระเงิน</h2>
          </div>
            <br/>
          <div class="col-md-12">

            <form  method="post" enctype="multipart/form-data">

              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">เลขที่ใบสั่งซื้อ <span class="text-danger">*</span></label>
										<select class="form-control" name="_refid" required=required id="_refid" onchange="selectOnchange()" >
											<option value="">เลือกใบสั่งซื้อ</option>

											<?php

												$index =0;
																					foreach ($select_all1 as $row) {
																							$index++;

																							?>
											<option value="<?php echo $row['refid'];?>"><?php echo $row['refid'];?></option>

											<?php
																 }
														 ?>
										</select>
                    <!-- <input type="text"  autocomplete="off"  class="form-control" id="name" name="_refid" required="required" maxlength="255"> -->
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_email" class="text-black">ยอดที่โอน <span class="text-danger">*</span></label>
                    <input type="text"  autocomplete="off"  class="form-control txtNumber" name="_amount1" id="_amount" required="required" maxlength="10" disabled>
										<input type="hidden" class="form-control txtNumber" name="_amount" id="_amount1" required="required" maxlength="10" >
									  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_message" class="text-black">วันที่โอน <span class="text-danger">*</span></label>
                    <input type="text"  autocomplete="off"  class="form-control txtNumber" name="_paydate" required="required" maxlength="50" value="<?php echo $date; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">เวลาที่โอน <span class="text-danger">*</span></label>
                    <input type="text"  autocomplete="off"  class="form-control" id="c_subject" name="_paytime" required="required" maxlength="5" value="<?php echo $time; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">แนปสลิป <span class="text-danger">*</span></label>
                    <input type="file" class="form-control" id="_slip" name="_slip" required="required" accept="image/png,image/jpeg">
                  </div>
                </div>


                <div class="form-group row">
                  <div class="col-lg-12">
                    <input type="submit" class="btn btn-black btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="btnsave" value="บันทึก" >
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

	<script type="text/javascript">


	$(document).ready(function() {
		// var date_input=$('input[name="_paydate"]'); //our date input has the name "date"
		// var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		// var options={
		// 	dateFormat: 'yy-mm-dd',
		// 	container: container,
		// 	todayHighlight: true,
		// 	autoclose: true,
		// };
		// date_input.datepicker(options);
		var date_input=$('input[name="_paydate"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		var options={
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		};
		date_input.datepicker(options);
	});

    $(document).on('keypress', '.txtNumber ', function (event) {
        	    console.log(event.charCode);
        	    event = (event) ? event : window.event;
        	    var charCode = (event.which) ? event.which : event.keyCode;
        	    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        	        return false;
        	    }
        	    return true;
        	});

					function selectOnchange() {

						var refid = document.getElementById('_refid').value;

						$.ajax({
								url: "getData.php",//ที่อยู่ของไฟล์เป้าหมาย
								global: false,
								type: "GET",//รูปแบบข้อมูลที่จะส่ง
								data: ({ID : refid}), //ข้อมูลที่ส่ง  { ชื่อตัวแปร : ค่าตัวแปร }
								dataType: "JSON", //รูปแบบข้อมูลที่ส่งกลับ xml,script,json,jsonp,text
								async:false,
								success: function(jd) { //แสดงข้อมูลเมื่อทำงานเสร็จ โดยใช้ each ของ jQuery
										document.getElementById('_amount').value = jd[0]["total"];
											document.getElementById('_amount1').value = jd[0]["total"];
									}
						});

					}
        	</script>

  </body>
</html>
