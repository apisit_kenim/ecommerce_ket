<?php

require_once __DIR__ . "../../../../vendor/autoload.php";
session_start();
ob_start();

require_once "../../../ConnectDatabase/connectionDb.inc.php";

$cmd = getIsset("__cmd");

$sql = "SELECT * FROM products ";
$select_all = $conn->queryRaw($sql);
$total = sizeof($select_all);


?>

<html>

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัดธาดาเซรามิก &mdash; </title>
</head>

<body>
  <br>
  <div style="text-align:center"> <b>ห้างหุ้นส่วนจำกัดธาดาเซรามิก </b><br>
  288 ม.9 ต.ทุ่งฝาย อ.เมือง จ.ลำปาง 52000 <br>โทร. 062-297-7421

  </div>
  <hr>
  <br>
  <div style="text-align:center">รายงานข้อมูลสินค้าคงเหลือ
  </div>
  <!-- <div style="text-align:center"></div> -->
  <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#424242">
    <tr>
      <th width="80" bgcolor="#D5D5D5">
        <div align="center">ลำดับ </div>
      </th>
      <th width="100" bgcolor="#D5D5D5">
        <div align="center">รหัสสินค้า </div>
      </th>
      <th width="200" bgcolor="#D5D5D5">
        <div align="center">ชื่อสินค้า</div>
      </th>
      <th width="200" bgcolor="#D5D5D5">
        <div align="center">จำนวนคงเหลือ </div>
      </th>
    </tr>

    </tr>
    <?php

    $index = 0;
    foreach ($select_all as $row) {
      $index++;
    ?>
      <tr>
        <td align="center"> <?php echo $index; ?></td>
        <td align="center"> <?php echo $row['productscode']; ?> </td>
        <td align="center"> <?php echo $row['productsname']; ?> </td>
        <td align="center"> <?php echo $row['amount']; ?> </td>
      </tr>
    <?php
    }
    ?>
  </table>

</body>

</html>

<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf = new \Mpdf\Mpdf([
  'default_font_size' => 16,
  'default_font' => 'sarabun'
]);

$mpdf->WriteHTML($html);
$mpdf->Output();

?>