<?php

require_once __DIR__ . "../../../../vendor/autoload.php";
session_start();
ob_start();

require_once "../../../ConnectDatabase/connectionDb.inc.php";


$datenow = date("Y-m-d");

$cmd = getIsset("__cmd");
$startdate = getIsset("_startdate");
$enddate = getIsset("_enddate");

$sql = "SELECT  productid,od.price,sum(od.qty) as qtys , sum(od.total) as totals FROM orders o INNER JOIN orderdetails od ON o.orderid = od.orderid  
WHERE data BETWEEN '$startdate' AND '$enddate' AND  paystatus = 'ชำระเงินแล้ว'  group by od.productid,od.price order by qtys desc";
$select_all = $conn->queryRaw($sql);
$total = sizeof($select_all);



?>

<html>

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
</head>

<body>
  <br>
  <div style="text-align:center"> <b>ห้างหุ้นส่วนจำกัดธาดาเซรามิก </b><br>
  288 ม.9 ต.ทุ่งฝาย อ.เมือง จ.ลำปาง 52000 <br>โทร. 062-297-7421

  </div>
  <hr>
  <br>
  <div style="text-align:center">รายงานข้อมูลการขาย
    ระหว่างวันที่ <?php echo $startdate; ?> ถึงวันที่ <?php echo $enddate; ?>
  </div>
  <!-- <div style="text-align:center"></div> -->
  <table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#424242">
    <tr>
      <th width="80" bgcolor="#D5D5D5">
        <div align="center">ลำดับ </div>
      </th>
      <th width="100" bgcolor="#D5D5D5">
        <div align="center">ชื่อสินค้า </div>
      </th>
      <th width="200" bgcolor="#D5D5D5">
        <div align="center">ราคา</div>
      </th>
      <th width="200" bgcolor="#D5D5D5">
        <div align="center">จำนวน </div>
      </th>
      <th width="120" bgcolor="#D5D5D5">
        <div align="center">รวม</div>
      </th>
    </tr>

    </tr>
    <?php

    $index = 0;
    foreach ($select_all as $row) {
      $index++;
      $objResult = $conn->select('products', array('productid' => $row['productid']), true);

      $SumCount = $SumCount + $row['qtys'];
      $SumTotal = $SumTotal + $row['totals'];
    ?>
      <tr>
        <td align="center"> <?php echo $index; ?></td>
        <td align="center"> <?php echo $objResult['productsname']; ?> </td>
        <td align="center"> <?php echo number_format($row['price'], 2); ?></td>
        <td align="center"> <?php echo number_format($row['qtys'], 2); ?></td>
        <td align="center"> <?php echo number_format($row['totals'], 2); ?></td>
      </tr>
    <?php
    }
    ?>
  </table>

</body>

</html>

<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf = new \Mpdf\Mpdf([
  'default_font_size' => 16,
  'default_font' => 'sarabun'
]);

$mpdf->WriteHTML($html);
$mpdf->Output();

?>