<?php

session_start();
require_once "../../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$sql = "SELECT * FROM products";
$select_all = $conn->queryRaw($sql);
$total = sizeof($select_all);

$sql = "SELECT *  from productstype ";
$tbl_productstype = $conn->queryRaw($sql);
$total = sizeof($tbl_productstype);

// $s = 0;
// $m = 0;
// $l = 0;
// $xl = 0;
// $xxl = 0;
// $amount = 0;

$sqlCus = "SELECT * FROM member";
$select_allCus = $conn->queryRaw($sqlCus);
$totalCus = sizeof($select_allCus);

if (intval($id) > 0) {
  $tbl_order = $conn->select('orders', array('orderid' => getIsset('id')), true);

  if ($tbl_order != null) {
    $tbl_customer = $conn->select('member', array('memberid' => $tbl_order["id_cus"]), true);
    $fullname = $tbl_customer["name"] . ' ' . $tbl_customer["lname"];
    $fulladdress = $tbl_customer["address"] . ' ต.' . $tbl_customer["district"] . ' อ.' . $tbl_customer["amphoe"] . ' จ.' . $tbl_customer["province"];
    $phoneNo = $tbl_customer["phone"];
  }

  $sql = "SELECT *  from orderdetails WHERE orderid = $id";
  $tbl = $conn->queryRaw($sql);
  $total = sizeof($tbl);

  $title = 'รายการขายส่ง';
} else {
  $title = 'เพิ่มรายการขายส่ง';

  if ($cmd == 'save') {
    $refid = substr(date("Y"), 2, 2) . date("md") . rand(1000, 10000); //Format ปี 2 หลัก เดือนวัน แล้ว random 4 หลัก

    $value = array(
      "refid" => $refid, "id_cus" => getIsset("_CusID"), "data" => date("Y-m-d H:i:s"), "paystatus" => 'ชำระเงินแล้ว', "statusnow" => 'ขายส่ง', "sent_rate" => '', "transport" => 'รับเองที่ร้าน'
    );


    if ($conn->create("orders", $value)) {
      $lastID = $conn->getLastInsertId();

      for ($i = 0; $i <= getIsset(countTable); $i++) {
        if ($_POST["txtName" . $i] != "" && $_POST["txtQty" . $i] != "") {
          //insert detail
          insertSellDetail($lastID, $_POST["txtProID" . $i], $_POST["txtQty" . $i], $_POST["txtPrice" . $i], $_POST["txtTotal" . $i]);
        }
      }
    }
    alertMassageAndRedirect('ทำรายการเรียบร้อยแล้ว', 'SellList.php');
  }
}

if ($cmd == 'delete') {
  //delete
  if ($conn->delete("products", array("productid" => $id))) {
    alertMassageAndRedirect('ลบข้อมูลเรียบร้อยแล้ว', 'productsList.php');
  } else {
    alertMassageAndRedirect('ไม่สามารถลบข้อมูลได้', 'productsList.php');
  }
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <?php include "../Menu/navbar.php" ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <?php include "../Menu/SidebarMenu.php" ?>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $title ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="box-body">
                  <input type="hidden" name="_id" value="<?php echo $id ?>">

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">ชื่อ - สกุล</label>
                    <div class="col-sm-10">

                      <input type="hidden" class="form-control" id="CusID" name="_CusID" placeholder="รหัสลูกค้า" value="<?php echo $CusID ?>">
                      <input type="text" class="form-control" id="name" name="name" placeholder="ชื่อ - สกุล" value="<?php echo $fullname ?>" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">ที่อยู่</label>

                    <div class="col-sm-10">
                      <input type="text" autocomplete="off" class="form-control" id="address" name="address" placeholder="ที่อยู่" value="<?php echo $fulladdress ?>" readonly>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">เบอร์โทร</label>

                    <div class="col-sm-10">
                      <input type="text" autocomplete="off" class="form-control" id="phone" name="phone" placeholder="เบอร์โทร" value="<?php echo $phoneNo ?>" readonly>
                    </div>
                  </div>
                  <hr>
                  <br>
                  <div class="box-body">
                    <div class="form-group">
                      <div class="col-12 col-md-10">
                        <h4>ข้อมูลการขาย</h4>
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-hover" name="_table" id="myTable">
                        <thead>
                          <tr>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>ราคาต่อหน่วย</th>
                            <th>จำนวน</th>
                            <th>รวม</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (is_array($tbl) || is_object($tbl)) {
                            $index = 0;
                            foreach ($tbl as $row) {
                              $index++;
                              $SumTotal = $SumTotal + $row['total'];
                              $tbl_product = $conn->select('products', array('productid' => $row["productid"]), true);
                          ?>

                              <tr>
                                <td> <input class="form-control" type="text" name="txtName<?php echo $index ?>" id="txtName<?php echo $index ?>" value="<?php echo $tbl_product['productscode']; ?>" readonly></td>
                                <td> <input class="form-control" type="text" name="txtGrade<?php echo $index ?>" id="txtGrade<?php echo $index ?>" value="<?php echo $tbl_product['productsname']; ?>" readonly></td>
                                <td> <input class="form-control" type="text" name="txtPrice<?php echo $index ?>" id="txtPrice<?php echo $index ?>" value="<?php echo $row['qty']; ?>" OnKeyUp="calculateAmount(<?php echo $index ?>) " readonly></td>
                                <td> <input class="form-control" type="number" name="txtQty<?php echo $index ?>" id="txtQty<?php echo $index ?>" value="<?php echo $row['price']; ?>" OnKeyUp="calculateAmount(<?php echo $index ?>)" readonly></td>
                                <td> <input class="form-control theClassName" type="text" name="txtTotal<?php echo $index ?>" id="txtTotal<?php echo $index ?>" value="<?php echo $row['total']; ?>" readonly></td>
                              </tr>
                          <?php
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                      <table class="table table-striped table-bordered table-hover" name="_table" id="myTable1">
                        <tr>
                          <td><b>ยอดรวม</b></td>
                          <td colspan="6"></td>
                          <td><input type="text" class="form-control" style="text-align:right" id="subtotal" name="_subtotal" placeholder="" value="<?php echo round($SumTotal) ?>" readonly></td>
                          <td><b>บาท</b></td>
                        </tr>
                        <!-- <tr>
                                                    <td><b>ภาษี</b></td>
                                                    <td colspan="6"></td>
                                                    <td><input type="text" class="form-control" style="text-align:right" id="vat" name="_vat" placeholder="" value="<?php echo round($Vat) ?>" readonly></td>
                                                    <td><b>บาท</b></td>
                                                </tr>
                                                <tr>
                                                    <td><b>รวมทั้งสิ้น</b></td>
                                                    <td colspan="6"></td>
                                                    <td><input type="text" class="form-control" style="text-align:right" id="total" name="_total" placeholder="" value="<?php echo round($Total) ?>" readonly></td>
                                                    <td><b>บาท</b></td>
                                                </tr> -->
                      </table>
                    </div>

                    <div class="box-footer">
                      <center>
                        <button type="reset" class="btn btn-default" onclick="cancelOnclick()">ปิด</button>
                      </center>
                    </div>

                    <input type="text" id="countTable" name="countTable" value="<?php echo $total; ?>" hidden>

              </form>
            </div>
            <!-- /.box -->
          </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- Modal Customer -->
  <div class="modal fade" id="mediumModalCus" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediumModalLabel">ข้อมูลลูกค้า</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>ลำดับ</th>
                <th>ชื่อ - สกุล</th>
                <th>ที่อยู่</th>
                <th>ตำบล</th>
                <th>อำเภอ</th>
                <th>จังหวัด</th>
                <th>เบอร์โทร</>
                <th>เลือก</th>
              </tr>
            </thead>
            <tbody>
              <?php

              $index = 0;
              foreach ($select_allCus as $row) {
                $index++;

              ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td><?php echo $row['name']; ?> <?php echo $row['lname']; ?></td>
                  <td><?php echo $row['address']; ?></td>
                  <td><?php echo $row['district']; ?></td>
                  <td><?php echo $row['amphoe']; ?></td>
                  <td><?php echo $row['province']; ?></td>
                  <td><?php echo $row['phone']; ?></td>
                  <td align="center"> <button type="button" class="btn btn-info" data-dismiss="modal" onclick="addCus('<?php echo $row['memberid']; ?>','<?php echo $row['name']; ?>','<?php echo $row['lname']; ?>','<?php echo $row['address']; ?>','<?php echo $row['district']; ?>','<?php echo $row['amphoe']; ?>','<?php echo $row['province']; ?>','<?php echo $row['phone']; ?>')">เลือก</button>
                  </td>
                </tr>

              <?php
              }
              ?>

            </tbody>
          </table>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Product -->
  <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediumModalLabel">ข้อมูลสินค้า</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>ลำดับ</th>
                <th>รหัสสินค้า</th>
                <th>ชื่อสินค้า</th>
                <th>ราคา</th>
                <th>เลือก</th>
              </tr>
            </thead>
            <tbody>
              <?php

              $index = 0;
              foreach ($select_all as $row) {
                $index++;

              ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td><?php echo $row['productscode']; ?></td>
                  <td><?php echo $row['productsname']; ?></td>
                  <td><?php echo $row['productsprice']; ?></td>
                  <td align="center"> <button type="button" class="btn btn-info" data-dismiss="modal" onclick="addMat('<?php echo $row['productid']; ?>','<?php echo $row['productscode']; ?>','<?php echo $row['productsname']; ?>','<?php echo $row['productsprice']; ?>')">เลือก</button>
                  </td>
                </tr>

              <?php
              }
              ?>

            </tbody>
          </table>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal -->

  <!-- jQuery 3 -->
  <script src="../../bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
  <!-- page script -->
  <script type="text/javascript">
    $(document).ready(function() {
      // checkProductType();
    });


    $(document).on('keypress', '.txtNumber ', function(event) {
      console.log(event.charCode);
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
      }
      return true;
    });

    function cancelOnclick() {
      window.location = 'SellList.php';
    }

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-upload').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readURL2(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-upload2').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readURL3(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-upload3').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readURL4(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#img-upload4').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    function addCus(id, name, lname, address, district, amphoe, province, phone) {
      var fullname = name + ' ' + lname
      var fulladdress = address + ' ต.' + district + ' อ.' + amphoe + ' จ.' + province;
      document.getElementById("CusID").value = id;
      document.getElementById("name").value = fullname;
      document.getElementById("address").value = fulladdress;
      document.getElementById("phone").value = phone;
    }

    function addMat(ProID, Name, Grade, Price) {

      var table = document.getElementById("myTable");
      table.insertRow(table.rows.length).innerHTML =
        '<tr><td> <input class="form-control" type="text" name="txtName' + table.rows.length + '" value="' + Name + '" readonly></td>' +
        '<td><input class="form-control" type="text" name="txtGrade' + table.rows.length + '" id="txtGrade' + table.rows.length + '" value="' + Grade + '" readonly ></td>' +
        '<td><input class="form-control" type="number" name="txtPrice' + table.rows.length + '" id="txtPrice' + table.rows.length + '" value="' + Price + '" OnKeyUp="calculateAmount(' + table.rows.length + ')" " readonly></td>' +
        '<td><input class="form-control" type="number" name="txtQty' + table.rows.length + '" id="txtQty' + table.rows.length + '" value="1" OnKeyUp="calculateAmount(' + table.rows.length + ')""></td>' +
        '<td><input class="form-control theClassName" type="number" name="txtTotal' + table.rows.length + '" id="txtTotal' + table.rows.length + '" value="' + Price + '" readonly></td>' +
        '<td align="center" ><button class="deleteDep" value="Delete" onclick="myFunction(' + table.rows.length + ')">X</td>' +
        '<tr><td> <input class="form-control" type="hidden" name="txtProID' + table.rows.length + '" value="' + ProID + '"></td>';

      document.getElementById("countTable").value = table.rows.length;

      Calculate();
    }

    function calculateAmount(id) {

      var nameQty = 'txtQty' + id;
      var namePrice = 'txtPrice' + id;
      var nameTotal = 'txtTotal' + id;

      if (nameQty != 'undefined' && namePrice != 'undefined') {
        var qty = document.getElementById(nameQty).value;
        var price = document.getElementById(namePrice).value;
        var total = qty * price;
        document.getElementById(nameTotal).value = total.toFixed(2);
      }
      Calculate();
    }

    function Calculate() {
      var table = document.getElementById('myTable');

      var items = document.getElementsByClassName("theClassName");

      var sum = 0;
      var total = 0;
      for (var i = 0; i < items.length; i++) {
        var str = "";
        sum += parseFloat(items[i].value);
      }

      var output = document.getElementById('subtotal');
      output.value = Math.round(sum.toFixed(2));

      // var vat = sum * 7 / 100;
      // var output = document.getElementById('vat');
      // output.value = Math.round(vat.toFixed(2));

      // var total = sum + vat;
      // var output = document.getElementById('total');
      // output.value = Math.round(total.toFixed(2));

    }

    function myFunction(id) {
      $('body').on('click', 'button.deleteDep', function() {
        $(this).parents('tr').remove();
      });
    }

    // function calculate() {
    //   var s = document.getElementById('txts').value;
    //   var m = document.getElementById('txtm').value;
    //   var l = document.getElementById('txtl').value;
    //   var xl = document.getElementById('txtxxl').value;
    //   var xxl = document.getElementById('txtxl').value;
    //
    //   if (s == "")
    //       s = 0;
    //
    //   if (m == "")
    //       m = 0;
    //
    //   if (l == "")
    //       l = 0;
    //
    //   if (xl == "")
    //       xl = 0;
    //
    //   if (xxl == "")
    //     xxl = 0;
    //
    //         document.getElementById('txtamount').value =  parseInt(s) + parseInt(m) + parseInt(l) + parseInt(xl) + parseInt(xxl);
    // }



    // function checkProductType(){
    //   var strUser = document.getElementById("producttype").value;
    //
    //   var x = document.getElementById("divSize");
    //
    //   if (strUser == 'กระเป๋า' || strUser == 'กระเป๋าพื้นเมือง' || strUser == 'ย่ามทอมือ' || strUser == 'ผ้าซิ่นตีนจก'|| strUser == 'กางเกงสดอ'|| strUser == 'กระโปรงม้ง'){
    //        x.style.display = "none";
    //        document.getElementById("txtamount").readOnly = false;
    //     }else{
    //        document.getElementById("txtamount").readOnly = true;
    //        x.style.display = "block";
    //     }
    //   }

    // function selectSizeOnClick(){
    //   var e = document.getElementById("_type");
    //   var strUser = e.options[e.selectedIndex].text;
    //
    //   var x = document.getElementById("divSize");
    //
    //   if (strUser == 'กระเป๋า' || strUser == 'กระเป๋าพื้นเมือง' || strUser == 'ย่ามทอมือ' || strUser == 'ผ้าซิ่นตีนจก'|| strUser == 'กางเกงสดอ'|| strUser == 'กระโปรงม้ง'){
    //        x.style.display = "none";
    //        document.getElementById("txtamount").readOnly = false;
    //     }else{
    //        document.getElementById("txtamount").readOnly = true;
    //        x.style.display = "block";
    //     }
    //   }
  </script>
</body>

</html>