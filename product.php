<?php

session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

//select all producttype
$sql = "SELECT *  from productstype ";

$select_all = $conn->queryRaw($sql);

$total = sizeof($select_all);


$typeid = getIsset("type");
$search = getIsset("__cmd");
$keyword = getIsset("_searchProduct");

if ($search == 'search') {

  $typeSearch = getIsset("Search_");

  if ($typeSearch == 'All') {
    $sql = "SELECT *  from products WHERE productsname LIKE '%$keyword%' or productsprice LIKE '%$keyword%' ";
  } else if ($typeSearch == 'Name') {
    $sql = "SELECT *  from products WHERE productsname LIKE '%$keyword%'";
  } else if ($typeSearch == 'Price') {
    $sql = "SELECT *  from products WHERE productsprice LIKE '%$keyword%'";
  } else {
    $sql = "SELECT *  from products WHERE productsname LIKE '%$keyword%'";
  }

  $tbl_products = $conn->queryRaw($sql);
} else if (intval($typeid) > 0) {
  $sql = "SELECT *  from products WHERE productstypecode = $typeid";
  $tbl_products = $conn->queryRaw($sql);
} else {
  $sql = "SELECT *  from products ";
  $tbl_products = $conn->queryRaw($sql);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>


    <div class="site-section">
      <div class="container">

        <div class="row mb-5">
          <div class="col-md-9 order-1">

            <div class="row align">
              <div class="col-md-12 mb-5">
                <div class="float-md-left">
                  <h2 class="text-black h5">สินค้า</h2>
                </div>
                <div class="d-flex">
                  <!-- <div class="dropdown mr-1 ml-md-auto">
                    <button type="button" class="btn btn-white btn-sm dropdown-toggle px-4" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Latest
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                      <a class="dropdown-item" href="#">Men</a>
                      <a class="dropdown-item" href="#">Women</a>
                      <a class="dropdown-item" href="#">Children</a>
                    </div>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-white btn-sm dropdown-toggle px-4" id="dropdownMenuReference" data-toggle="dropdown">Reference</button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
                      <a class="dropdown-item" href="#">Relevance</a>
                      <a class="dropdown-item" href="#">Name, A to Z</a>
                      <a class="dropdown-item" href="#">Name, Z to A</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Price, low to high</a>
                      <a class="dropdown-item" href="#">Price, high to low</a>
                    </div>
                  </div> -->
                </div>
              </div>
            </div>
            <div class="row mb-5">

              <?php
              $index = 0;
              foreach ($tbl_products as $row) {
                $index++;

              ?>

                <div class="col-lg-6 col-md-6 item-entry mb-4">
                  <a href="product-detail.php?id=<?php echo $row["productid"]; ?>" class="product-item md-height bg-gray d-block">
                    <img src="images/<?php echo $row["productsphoto"]; ?>" alt="Image" class="img-fluid">
                  </a>
                  <h2 class="item-title"><a href="product-detail.php?id=<?php echo $row["productid"]; ?>"><?php echo $row["productsname"]; ?></a></h2>
                  <strong class="item-price">฿ <?php echo $row["productsprice"]; ?> บาท</strong>
                </div>

              <?php
              }
              ?>

            </div>
            <!-- <div class="row">
              <div class="col-md-12 text-center">
                <div class="site-block-27">
                  <ul>
                    <li><a href="#">&lt;</a></li>
                    <li class="active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                  </ul>
                </div>
              </div>
            </div> -->
          </div>

          <div class="col-md-3 order-2 mb-5 mb-md-0">
            <div class="border p-4 rounded mb-4">
              <h3 class="mb-3 h6 text-uppercase text-black d-block">ประเภทสินค้า</h3>
              <hr>
              <ul class="list-unstyled mb-0">
                <?php

                $index = 0;
                foreach ($select_all as $row) {
                  $index++;

                ?>
                  <li class="mb-1"><a href="product.php?type=<?php echo $row['productstypeid']; ?>" class="d-flex text-black"><span><?php echo $row['productstypename']; ?></span> </a></li>
                <?php
                }
                ?>
              </ul>
            </div>

            <div class="border p-4 rounded mb-4">
              <div class="mb-4">
                <h3 class="mb-3 h6 text-uppercase text-black d-block">ค้นหา</h3>

                <select class="selection-2" name="Search_" id="Search_">
                  <option value="All">ค้นหาทั้งหมด</option>
                  <option value="Name">ค้นหาจากชื่อสินค้า</option>
                  <option value="Price">ค้นหาจากราคาสินค้า</option>
                </select>

                <br />
                <br />

                <input class="form-control" type="text" autocomplete="off" name="_searchProduct" id="_searchProduct" placeholder="Search..." required="required" max="255">

                <br />

                <button type="button" class="btn btn-black btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="__cmd" value="search" onclick="searchOnClick()">
                  ค้นหา
                </button>
              </div>

              <!-- <div class="mb-4">
                <h3 class="mb-3 h6 text-uppercase text-black d-block">Size</h3>
                <label for="s_sm" class="d-flex">
                  <input type="checkbox" id="s_sm" class="mr-2 mt-1"> <span class="text-black">Small (2,319)</span>
                </label>
                <label for="s_md" class="d-flex">
                  <input type="checkbox" id="s_md" class="mr-2 mt-1"> <span class="text-black">Medium (1,282)</span>
                </label>
                <label for="s_lg" class="d-flex">
                  <input type="checkbox" id="s_lg" class="mr-2 mt-1"> <span class="text-black">Large (1,392)</span>
                </label>
              </div>

              <div class="mb-4">
                <h3 class="mb-3 h6 text-uppercase text-black d-block">Color</h3>
                <a href="#" class="d-flex color-item align-items-center" >
                  <span class="bg-danger color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Red (2,429)</span>
                </a>
                <a href="#" class="d-flex color-item align-items-center" >
                  <span class="bg-success color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Green (2,298)</span>
                </a>
                <a href="#" class="d-flex color-item align-items-center" >
                  <span class="bg-info color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Blue (1,075)</span>
                </a>
                <a href="#" class="d-flex color-item align-items-center" >
                  <span class="bg-primary color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Purple (1,075)</span>
                </a>
              </div> -->

            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  <script type="text/javascript">
    function searchOnClick() {
      var Search = document.getElementById('Search_');

      window.location = 'product.php?__cmd=search&_searchProduct=' + document.getElementsByName("_searchProduct")[0].value + '&Search_=' + Search.options[Search.selectedIndex].value;
    }
  </script>

</body>

</html>