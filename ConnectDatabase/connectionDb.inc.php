  <?php

define('DB_HOST','localhost');
define('DB_NAME','db_ecommerce_ketkanlaya');
define('DB_USERNAME','root');
define('DB_PASSWORD','root');

//host
// define('DB_HOST','localhost');
// define('DB_NAME','tadacera_db');
// define('DB_USERNAME','tadacera_db');
// define('DB_PASSWORD','P7szGoisyb');

define('ROOTPATH',curRootPath('admin'));

// Create connection
require_once('MySQLDBConn.class.php');
$conn = new MySQLDBConn(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);

function getDbConnection() {
    return new MySQLDBConn(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
}

function curRootPath($localhost_path,$server_name='localhost')
{
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"])) if($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    //$server_name = $_SERVER["SERVER_NAME"];
    if ($_SERVER["SERVER_PORT"] != "80")
    {
        $pageURL .= $server_name.":".$_SERVER["SERVER_PORT"].'/'.$localhost_path;
    }
    else if($_SERVER["SERVER_PORT"] == "80" && ($server_name == 'localhost' || $server_name == '127.0.0.1')){
        $pageURL .= $server_name.'/'.$localhost_path;
    }
    else
    {
        $pageURL .= $server_name;
    }
    return $pageURL;
}
// echo "<script> alert('Please check username and password'); history.back(); </script>";

// echo "Connected successfully";
function getIsset($post_value){
    $value="";
    if(isset($_GET[$post_value])){
        $value = $_GET[$post_value];
    }
    if(isset($_POST[$post_value])){
        $value = $_POST[$post_value];
    }
    return $value;
}

function redirectTo($url){
  header('location: '.$url);
  exit(0);
}

function alertMassage($str){
    echo "<script>alert('".$str."');</script>";
}

function alertMassageAndRedirect($str,$url){
  	echo "<script> alert('".$str."'); location.href='".$url."'; </script>";
}

function alertMassageAndRedirect1($str,$url){
  	echo "<script> location.href='".$url."' , _blank ; </script>";
}

function confirmMassage($str){
    echo "<script>confirm('".$str."');</script>";
}

function generate_action_tag($type,$message){
    $tag = '';
    if('success' == $type){
        $tag = '<div class="alert alert-success alert-dismissible">
                        <h4><i class="icon fa fa-check"></i>'.$message.'</h4>
                    </div>';
    }
    if('error' == $type){
        $tag = '<div class="alert alert-danger alert-dismissible">
                        <h4><i class="icon fa fa-ban"></i>'.$message.'</h4>
                    </div>';
    }
    return $tag;
}

function inserOrderDetail($orderid,$productid,$qty,$price,$total,$size) {
    $conn = getDbConnection()->getConnection();
    $sql = "INSERT INTO orderdetails (orderid,productid,qty,price,total,size) VALUES ('$orderid','$productid','$qty','$price','$total','$size')";
      $conn->query("SET NAMES UTF8");
      $conn->query($sql);
}


function updateStock($productid,$qty) {

  $conn = getDbConnection()->getConnection();
  $sql = "UPDATE products SET amount = '$qty' WHERE productid = $productid";
    $conn->query("SET NAMES UTF8");
  $conn->query($sql);
}

function updateProduct($new_image_name,$new_image_name2,$new_image_name3,$new_image_name4,$code,$name,$type,$price,$typeProduct,$detail,$amount,$id) {
  $conn = getDbConnection()->getConnection();

if ($id != ""){
  $sql = "UPDATE products SET productscode = '$code' , productsname = '$name', productstypecode = '$type',   productsprice = '$price',readprd = '$typeProduct',productsdetail = '$detail',  amount = '$amount' ";
  }

  if ($new_image_name != "" ){
      $sql .=  ",productsphoto = '$new_image_name'";
  }

  if ($new_image_name2 != "" ){
  $sql .=  ",productsphoto2 = '$new_image_name2'";
  }

  if ($new_image_name3 != "" ){
    $sql .= ",productsphoto3 = '$new_image_name3'";
  }

  if ($new_image_name4 != "" ){
    $sql .=  ",productsphoto4 = '$new_image_name4'";
  }

  $sql .= " WHERE productid = $id";
  $conn->query("SET NAMES UTF8");
   $conn->query($sql);
}

function updateStatusOrder($orderid,$status) {

  $conn = getDbConnection()->getConnection();
  $sql = "UPDATE orders SET statusnow = '$status' WHERE refid = $orderid";
    $conn->query("SET NAMES UTF8");
  $conn->query($sql);
}

function updateStatusConfirm($orderid) {

  $conn = getDbConnection()->getConnection();
  $sql = "UPDATE orders SET statusnow = 'รอจัดส่งสินค้า' , paystatus = 'ชำระเงินแล้ว' WHERE orderid = $orderid";
    $conn->query("SET NAMES UTF8");
  $conn->query($sql);
}

function updateStatusReject($orderid) {

  $conn = getDbConnection()->getConnection();
  $sql = "UPDATE orders SET statusnow = 'ตรวจสอบยอดชำระใหม่' , paystatus = 'รอชำระเงิน' WHERE orderid = $orderid";
    $conn->query("SET NAMES UTF8");
  $conn->query($sql);
}


function updateStatusConfirmTransport($orderid,$transport) {

  $conn = getDbConnection()->getConnection();
  $DateTime = date('Y-m-d H:i:s');
  $sql = "UPDATE orders SET statusnow = 'จัดส่งสินค้าเรียบร้อยแล้ว' , sent_rate = '$transport' , datesent = '$DateTime'  WHERE orderid = $orderid";
  $conn->query("SET NAMES UTF8");
  $conn->query($sql);
}

function convertDateThaiFormatToUtc($var) {

    $time = date("H:i:s",strtotime($var));
    $H = date("H",strtotime($var));
    $SH = $H + 7;
    $DateTime = date('Y-m-d', strtotime($var))." ".$SH.":".date("i:s",strtotime($var));
    return $DateTime;
}


//Order SELL
function insertSellDetail($orderid,$productid,$qty,$price,$total) {
  $conn = getDbConnection()->getConnection();
  $sql = "INSERT INTO orderdetails (orderid,productid,qty,price,total) VALUES ('$orderid','$productid','$qty','$price','$total')";
$conn->query("SET NAMES UTF8");
  $conn->query($sql);
}

function DeleteSellDetail($orderid) {
  $conn = getDbConnection()->getConnection();
  $sql = "DELETE From orderdetailsid WHERE orderid = $orderid";
  $conn->query($sql);
}

function updateProductStock($id,$s,$m,$l,$xl,$xxl) {
  // alertMassage('updateProductStock');
  $conn = getDbConnection()->getConnection();
  $sql = "UPDATE stock SET s = $s , m = $m, l = $l, xl = $xl, xxl = $xxl WHERE productid = $id";
  $conn->query($sql);

 }

 function updateStockProduct($productid,$qty,$type) {

   $conn = getDbConnection()->getConnection();

   if ($type == 's'){
     $sql = "UPDATE stock SET s = $qty WHERE productid = $productid";
   }else if ($type == 'm'){
     $sql = "UPDATE stock SET m = $qty WHERE productid = $productid";
   }else if ($type == 'l'){
     $sql = "UPDATE stock SET l = $qty WHERE productid = $productid";
   }else if ($type == 'xl'){
     $sql = "UPDATE stock SET xl = $qty WHERE productid = $productid";
   }else if ($type == 'xxl'){
     $sql = "UPDATE stock SET xxl = $qty WHERE productid = $productid";
   }
  $conn->query("SET NAMES UTF8");
   $conn->query($sql);
 }

 function getUrlContent($url) {
    fopen("cookies.txt", "w");
    $parts = parse_url($url);
    $host = $parts['host'];
    $ch = curl_init();
    $header = array('GET /1575051 HTTP/1.1',
        "Host: {$host}",
        'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language:en-US,en;q=0.8',
        'Cache-Control:max-age=0',
        'Connection:keep-alive',
        'Host:adfoc.us',
        'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36',
    );

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);

    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookies.txt');
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookies.txt');
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

?>
