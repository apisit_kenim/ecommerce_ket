<?php

session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");

if (intval($id) > 0) {

  $tbl_products = $conn->select('products', array('productid' => getIsset('id')), true);

  if ($tbl_products != null) {
    $productid = $tbl_products["productid"];
    $productstypecode = $tbl_products["productstypecode"];
    $productsocde = $tbl_products["productsocde"];
    $productsname = $tbl_products["productsname"];
    $productsprice = $tbl_products["productsprice"];
    $productsdetail = $tbl_products["productsdetail"];
    $productsphoto = $tbl_products["productsphoto"];
    $productsphoto2 = $tbl_products["productsphoto2"];
    $productsphoto3 = $tbl_products["productsphoto3"];
    $productsphoto4 = $tbl_products["productsphoto4"];
    $readprd = $tbl_products["readprd"];

    $amount = $tbl_products["amount"];

    $tbl_productstype = $conn->select('productstype', array('productstypeid' => $productstypecode), true);
    $typeName = $tbl_productstype["productstypename"];
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <form method="post" action="add_order.php?ProductID=<?php echo $productid; ?>">

      <div class="site-section">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="item-entry">
                <a href="#" class="product-item md-height bg-gray d-block">
                  <img src="images/<?php echo $productsphoto; ?>" alt="Image" class="img-fluid">
                </a>
              </div>

            </div>
            <div class="col-md-6">
              <h2 class="text-black"><b><?php echo $productsname; ?></b></h2>
              <p><?php echo htmlspecialchars($productsdetail); ?></p>
              <p><strong class="text-black h5">฿<?php echo $productsprice; ?> บาท</strong></p>
              <p>จำนวนในสต๊อก : <?php echo $amount; ?></p>
              <hr>
              <div class="mb-5">
                <div class="input-group mb-3" style="max-width: 120px;">
                  <div class="input-group-prepend">
                    <button class="btn btn-outline-black js-btn-minus" style=" background-color: #AAA8A8; color: white;" type="button">&minus;</button>
                  </div>
                  <input type="text" autocomplete="off" class="form-control text-center" name="num-product" value="1" aria-label="Example text with button addon" aria-describedby="button-addon1" maxlength="3">
                  <div class="input-group-append">
                    <button class="btn btn-outline-black js-btn-plus" style=" background-color: #AAA8A8; color: white;" type="button">&plus;</button>
                  </div>
                </div>

              </div>
              <p><button type="submit" class="btn btn-sm btn-black">สั่งซื้อสินค้า</button></p>

            </div>
          </div>
        </div>
      </div>

    </form>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>

  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

</body>

</html>