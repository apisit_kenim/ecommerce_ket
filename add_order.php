<?php

session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

$xs = getIsset("xs");
$s = getIsset("s");
$m = getIsset("m");
$l = getIsset("l");
$xl = getIsset("xl");
$xxl= getIsset("xxl");
$size = getIsset("size") ? : '';
$count = getIsset("num-product") ? : 0;

if(!isset($_SESSION["intLine"]))
{
	//สั้งซื้อสินค้าครั้งแรก ยังไม่มีข้อมูลใน session
	$tbl_ = $conn->select('products', array('productid' => $_GET["ProductID"]), true);

	if($tbl_ != null){

		if (intval($count) <= intval($tbl_["amount"])){
			$_SESSION["intLine"] = 0;
			$_SESSION["strProductID"][0] = $_GET["ProductID"];
			$_SESSION["strQty"][0] = $count;
			$_SESSION["strSize"][0] = $size;

			header("location:cart.php");
		 }else{
			 alertMassageAndRedirect('จำนวนสินค้าในสต๊อคไม่เพียงพอ1','product.php');
		 }

	 } else{
		 $_SESSION["intLine"] = 0;
		 $_SESSION["strProductID"][0] = $_GET["ProductID"];
		 $_SESSION["strQty"][0] = $count;
		 $_SESSION["strSize"][0] = $size;

		header("location:cart.php");
	}
}
else
{
	// สั่งซื้อสินค้าครั้งต่อไป มีข้อมูลใน session แล้ว
	$tbl_ = $conn->select('products', array('productid' => $_GET["ProductID"]), true);

	if($tbl_ != null){

		if (intval($count) <= intval($tbl_["amount"])){

			$arrProductID = [];
			// Loop data in Cart
			for($i=0;$i<=(int)$_SESSION["intLine"];$i++)
			{
					// Group สินค้าชนิดเดียวกัน
					if ($_SESSION["strProductID"][$i] === $_GET["ProductID"]){
						array_push($arrProductID,$i);
					}
			}


				echo '<pre>' . print_r($arrProductID, TRUE) . '</pre>';

				$isAdd = true;


				if (sizeof($arrProductID) > 0){
						// Loop update Qty
						for($i=0;$i<=sizeof($arrProductID);$i++)
						{
							if ($_SESSION["strSize"][$arrProductID[$i]] == $_POST["size"]){
									$_SESSION["strQty"][$arrProductID[$i]] = $_SESSION["strQty"][$arrProductID[$i]] + $count;
									$_SESSION["strSize"][$arrProductID[$i]] = $size;

									$isAdd = false;
							}
						}

						// Add product
						if ($isAdd){
									$_SESSION["intLine"] = $_SESSION["intLine"] + 1;
									$intNewLine = $_SESSION["intLine"];
									$_SESSION["strProductID"][$intNewLine] = $_GET["ProductID"];
									$_SESSION["strQty"][$intNewLine] = $count;
									$_SESSION["strSize"][$intNewLine] = $size;
						}
				} else {
						// Add product condition not in cart
						$_SESSION["intLine"] = $_SESSION["intLine"] + 1;
						$intNewLine = $_SESSION["intLine"];
						$_SESSION["strProductID"][$intNewLine] = $_GET["ProductID"];
						$_SESSION["strQty"][$intNewLine] = $count;
					 	$_SESSION["strSize"][$intNewLine] = $size;
				}

				 header("location:cart.php");
 		 }else{
			 $id = $_GET["ProductID"];

			 alertMassageAndRedirect('จำนวนสินค้าในสต๊อคไม่เพียงพอ','product-detail.php?id='.$id);
		 }
	 }
}


?>
