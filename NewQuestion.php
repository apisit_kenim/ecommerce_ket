<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

		if($_REQUEST[btnsave])	{

			$value = array(
					"Question"=>getIsset('_question')
					 ,"Details"=>getIsset('_detail')
					 ,"Name"=>$_SESSION["memberName"]
					 ,"CreateDate"=>date("Y-m-d H:i:s")
					 ,"View"=>0
					 ,"Reply"=>0
			);

		 if($conn->create("webboard",$value)){
				alertMassageAndRedirect('ตั้งกระทู้เรียบร้อยแล้ว','Webboard.php');
		 }else{
				alertMassage("ไม่สามารถตั้งกระทู้ได้");
		 }
 }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>

  <div class="site-wrap">


    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="h3 mb-3 text-black">ตั้งกระทู้ใหม่</h2>
          </div>
          <div class="col-md-12">
            <form class="leave-comment" method="post">
          						<div class="bo4 of-hidden size15 m-b-20">
          							<input class="form-control" type="text"  autocomplete="off"  name="_question" placeholder="หัวข้อ" required="required" max="255">
          						</div>
                      <br>
          						<div class="bo4 of-hidden m-b-20">  <textarea
                        name="_detail"
                        class="form-control"
                        rows="10"
                        placeholder="รายละเอียด"
                        required  ><?php echo htmlspecialchars($productsdetail); ?></textarea>
                      </div>
                      <br>
                      <div class="m-text26 p-b-36 p-t-15" align="center">
          						<div class="w-size25">
          							<button class="btn-black btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="btnsave" value="true">
          								บันทึก
          							</button>
          						</div></div>
          					</form>
          </div>
        </div>
      </div>
    </div>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  </body>
</html>
