<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

if ($_REQUEST[btnregis] === 'สมัครสมาชิก') {
  $value = array(
    "name" => getIsset('_name'), "address" => getIsset('_address'), "zipcode" => getIsset('zipcode'), "phone" => getIsset('_phone'), "email" => getIsset('_email'), "username" => getIsset('_username'), "password" => getIsset('_password'), "isActive" => true, "district" => getIsset('district'), "amphoe" => getIsset('amphoe'), "province" => getIsset('province'), "lname" => getIsset('_lname')
  );

  $chk_isnew = $conn->select('member', array('username' => getIsset('_username')), true);

  if ($chk_isnew == null) {
    if ($conn->create("member", $value)) {
      alertMassageAndRedirect('Success', 'index.php');
    } else {
      alertMassage("Can not Use !");
    }
  } else {
    alertMassage('Can not Use ! ');
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/css/uikit.css">
  <link rel="stylesheet" href="jquery.Thailand.js/dist/jquery.Thailand.min.css">

</head>

<body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="h3 mb-12 text-black">สมัครสมาชิก</h2>
          </div>
          <br />
          <div class="col-md-12">

            <form id="demo1" method="post" autocomplete="off">

              <div class="p-3 p-lg-5 border">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">ชื่อ <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control" id="name" name="_name" required="required" maxlength="50">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_fname" class="text-black">นามสกุล <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control" id="lname" name="_lname" required="required" maxlength="50">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_email" class="text-black">เบอร์โทร <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control txtNumber" name="_phone" required="required" maxlength="10">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">อีเมล์ </label>
                    <input type="email" class="form-control" id="c_subject" name="_email" required="required" maxlength="50">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_message" class="text-black">ที่อยู่ <span class="text-danger">*</span></label>
                    <textarea id="c_message" cols="30" rows="7" class="form-control" name="_address" required="required" maxlength="100"></textarea>
                  </div>
                </div>
                <!--      <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">รหัสไปรษณีย์ <span class="text-danger">*</span></label>
                    <input type="text"  autocomplete="off"  class="form-control" id="c_subject" name="_zipcode" required="required" maxlength="5">
                  </div>
                </div> -->

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">ตำบล <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control" name="district" class="form-control" id="district" required>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">อำเภอ <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control" id="amphoe" name="amphoe" required>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">จังหวัด <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control" id="province" name="province" required>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">รหัสไปรษณีย์ <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control txtNumber" id="zipcode" name="zipcode" maxlength="5" required>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                    <input type="text" autocomplete="off" class="form-control" id="c_subject" name="_username" required="required" maxlength="10">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_subject" class="text-black">รหัสผ่าน <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" id="c_subject" name="_password" required="required" minlength="8" maxlength="10">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-12">
                    <input type="submit" class="btn btn-black btn-lg btn-block"  style=" background-color: #000000; color: white; border-color: black;" name="btnregis" value="สมัครสมาชิก">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>

  <!-- dependencies for zip mode -->
  <script type="text/javascript" src="jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
  <!-- / dependencies for zip mode -->

  <script type="text/javascript" src="jquery.Thailand.js/dependencies/JQL.min.js"></script>
  <script type="text/javascript" src="jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

  <script type="text/javascript" src="jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>


  <script type="text/javascript">
    $.Thailand({
      database: './jquery.Thailand.js/database/db.json',

      $district: $('#demo1 [name="district"]'),
      $amphoe: $('#demo1 [name="amphoe"]'),
      $province: $('#demo1 [name="province"]'),
      $zipcode: $('#demo1 [name="zipcode"]'),

      onDataFill: function(data) {
        console.info('Data Filled', data);
      },

      onLoad: function() {
        console.info('Autocomplete is ready!');
        $('#loader, .demo').toggle();
      }
    });

    $(document).ready(function() {
      // $.Thailand({
      //     database: './jquery.Thailand.js/database/db.json', // path หรือ url ไปยัง database
      //     $district: $('#district'), // input ของตำบล
      //     $amphoe: $('#amphoe'), // input ของอำเภอ
      //     $province: $('#province'), // input ของจังหวัด
      //     $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
      // });
    });

    $(document).on('keypress', '.txtNumber ', function(event) {
      console.log(event.charCode);
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
      }
      return true;
    });
  </script>

</body>

</html>