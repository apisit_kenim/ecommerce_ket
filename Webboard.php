<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

$sql = "select * from webboard ";

$select_allwebboard = $conn->queryRaw($sql);

$totalwebboard = sizeof($select_allwebboard);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <div class="site-wrap">


    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <!-- <div class="col-md-12"> -->
          <!-- <h2 class="h3 mb-3 text-black">เว็บบอร์ด</h2> -->
          <!-- </div> -->
          <div class="col-md-12">

            <div class="col-md-12 text-center">
              <div class="col-md-2 right">
                <a id="body_btnAdd" class="tn btn-black btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" href="NewQuestion.php?id=0">ตั้งกระทู้ใหม่</a>
              </div>
            </div>
            <br>

          </div>
          <div class="col-md-12">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ลำดับ</th>
                  <th>หัวข้อ</th>
                  <th>โดย</th>
                  <th>วัน/เวลา</th>
                  <th>อ่าน</th>
                  <th>ตอบ</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $index = 0;
                foreach ($select_allwebboard as $row) {
                  $index++;
                ?>
                  <tr>
                    <td>
                      <div align="center"><?php echo $index; ?></div>
                    </td>
                    <td><a href="ViewWebboard.php?id=<?php echo $row['QuestionID']; ?>" style="color: black;"><?php echo $row['Question']; ?></a></td>
                    <td><?php echo $row['Name']; ?></td>
                    <td>
                      <div align="center"><?php echo $row['CreateDate']; ?></div>
                    </td>
                    <td align="right"><?php echo $row['View']; ?></td>
                    <td align="right"><?php echo $row['Reply']; ?></td>
                  </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

</body>

</html>