<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");

if (intval($id) > 0) {
  $tbl_webboard = $conn->select('webboard', array('QuestionID' => getIsset('id')), true);

  if ($tbl_webboard != null) {
    $QuestionID = $tbl_webboard["QuestionID"];
    $CreateDate = $tbl_webboard["CreateDate"];
    $Question = $tbl_webboard["Question"];
    $Details = $tbl_webboard["Details"];
    $Name = $tbl_webboard["Name"];
    $View = $tbl_webboard["View"];
    $Reply = $tbl_webboard["Reply"];
  }

  $sql = "select * from reply where QuestionID = $id ";
  $tbl_reply = $conn->queryRaw($sql);
}

if ($_REQUEST[btnsave]) {
  $valueReply = array(
    "QuestionID" => $id, "Details" => getIsset('replyDetail'), "Name" => $_SESSION["memberName"], "CreateDate" => date("Y-m-d H:i:s")
  );

  if ($conn->create("reply", $valueReply)) {

    $countReply = intval($Reply) + 1;
    $conn = getDbConnection()->getConnection();
    $sql = "UPDATE webboard SET Reply = $countReply WHERE QuestionID = $id";
    $conn->query($sql);

    alertMassageAndRedirect('ตอบกลับกระทู้เรียบร้อยแล้ว', 'Webboard.php');
  } else {
    alertMassage("ไม่สามารถตอบกลับกระทู้ได้");
  }
} else {
  //update View
  $countView = intval($View) + 1;
  $conn1 = getDbConnection()->getConnection();
  $sql = "UPDATE webboard SET View = $countView WHERE QuestionID = $id";
  $conn1->query($sql);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet" href="css/font-awesome.min.css">

  <style>
    .panelWebboard {
      border: 1px solid #CCCCCC;
      padding: 20px;
    }
  </style>

</head>

<body>

  <div class="site-wrap">


    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="h3 mb-3 text-black">กระทู้<?php echo $Question; ?></h2>
          </div>
          <div class="col-md-12">
            <div class="bo4 of-hidden m-b-20">
              <div class="panelWebboard">
                <h3><?php echo $Question; ?></h3>
                <hr>
                <table width="100%">
                  <tr valign="middle">
                    <td width="180px"><strong>วันที่ : <?php echo $CreateDate; ?></strong>
                    <td><strong>โดย : <?php echo $Name; ?> </strong>
                    <td width="60px">
                    <td width="60px">
                </table>
                <br><br>
                <p><?php echo $Details; ?></p>
              </div>
            </div>
            <?php

            $index = 0;
            foreach ($tbl_reply as $row) {
              $index++;

            ?>
              <br>
              <div class="bo4 of-hidden m-b-20">
                <div class="panelWebboard">
                  <h3>ตอบกลับ#<?php echo $index; ?>
                    <?php echo $Question; ?></h3>
                  <hr>

                  <table width="100%">
                    <tr valign="middle">
                      <td width="180px"><strong>วันที่ : <?php echo $row['CreateDate']; ?></strong>
                      <td><strong>โดย : <?php echo $row['Name']; ?> </strong>
                      <td width="60px">
                      <td width="60px">
                  </table>
                  <br><br>
                  <p><?php echo $row['Details']; ?></p>
                </div>
              </div>
            <?php
            }
            ?>
            <?php if ($_SESSION["isLogin"] == '1') { ?>
              <br>
              <br>
              <form class="leave-comment" method="post">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4><i>Re: <?php echo $Question; ?></i></h4>
                  </div>
                  <div class="panelWebboard">

                    <script src="bower_components/ckeditor/ckeditor.js"></script>
                    <div class="form-group">
                      <textarea id="replyDetail" name="replyDetail" rows="10" class="form-control" required></textarea>
                    </div>
                    <div class="m-text26 p-b-36 p-t-15" align="center">
                      <button class="btn-black btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="btnsave" value="true">
                        บันทึก
                      </button>
                    </div>
                  </div>
                  <br>
                </div>
          </div>
          </form>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>


  <div class="bg-light py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mb-0"></div>
      </div>
    </div>
  </div>

  <footer class="site-footer custom-border-top">
    <?php include "Menu/footer.php" ?>
  </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

</body>

</html>