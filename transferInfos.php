<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <center>
                <h3>ธนาคารกสิกรไทย </h3>
            </center>
            <br>
            <center>
              <img src="images/gsb.png" alt="" width="150" height="150">
            </center>
            <br>
            <center>

              <h3>หมายเลขบัญชี 020-324-990-603</h3>
              <h3>ชื่อบัญชี เกตุกัลยา นะนันวี</h3>
            </center>
          </div>
        </div>
      </div>
    </div>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  <script>

    $(document).on('keypress', '.txtNumber ', function (event) {
        	    console.log(event.charCode);
        	    event = (event) ? event : window.event;
        	    var charCode = (event.which) ? event.which : event.keyCode;
        	    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        	        return false;
        	    }
        	    return true;
        	});
        	</script>

  </body>
</html>
