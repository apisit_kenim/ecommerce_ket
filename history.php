<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";
$memberid = $_SESSION["memberid"];

$sql = "SELECT * FROM orders where id_cus = $memberid order by orderid desc ";

$select_allwebboard = $conn->queryRaw($sql);

$totalwebboard = sizeof($select_allwebboard);

?>

 <!DOCTYPE html>
<html lang="en">
  <head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>

  <div class="site-wrap">

  <?php include "Menu/navbar.php" ?>

  <div class="bg-light py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mb-0"></div>
      </div>
    </div>
  </div>


    <div class="site-section">
      <div class="container">
        <div class="col-md-12">
          <label class="text-black h4" for="coupon">ประวัติการสั่งซื้อสินค้า</label>
        </div>

        <div class="row mb-5">
          <div class="col-md-12" >
            <div class="site-blocks-table">

              <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ลำดับ</th>
                <th>เลขที่ใบสั่งซื้อ</th>
                <th>วันที่สั่งซื้อ</th>
                <th>การจัดส่ง</th>
               <th>หมายเลขติดตาม</th>
                <th>สถานะ</th>
                <th>รายละเอียด</th>
                  <th>รับสินค้า</th>
            </tr>
            </thead>
            <tbody>
              <?php

                          $index =0;
                                  foreach ($select_allwebboard as $row) {
                                      $index++;

                                      ?>

                 <tr>
                   <td><div align="center"><?php echo $index; ?></div></td>
                   <td><?php echo $row['refid']; ?></a></td>
                   <td><?php echo $row['data']; ?></td>
                   <td><div align="center"><?php echo $row['transport']; ?></div></td>
                      <td><div align="center"><?php echo $row['sent_rate']; ?></div></td>
                   <td align="right"><?php echo $row['statusnow']; ?></td>
                   <td align="center">
                   <a onclick="editOnclick(<?php echo $row['orderid']; ?>)"><i class="icon-file-o"></i></a>
                  </td>
                  <?php if ($row['statusnow'] == 'จัดส่งสินค้าเรียบร้อยแล้ว') { ?>
                    <td align="center">
                    <button onclick="approveOnclick(<?php echo $row['orderid']; ?>)">รับสินค้า</button>
                   </td>
                  <?php } else { ?>
                    <td align="center">

                   </td>
                  <?php }?>
                 </tr>
                 <?php
                            }
                        ?>
                 </tbody>
          </table>

        </div>
          </div>
        </div>


      </div>
    </div>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  <script type="text/javascript">
        function editOnclick(id) {
            window.location = 'history_detail.php?id=' + id;
        }

        function approveOnclick(id) {
          if(confirm('ยืนยันการรับสินค้า ใช่ หรือ ไม่?')==true)
          {
            window.location = 'history_detail.php?id=' + id +'&__cmd=approve';
          }

        }
    </script>

  </body>
</html>
