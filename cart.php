<?php
session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

if(!isset($_SESSION["intLine"]))
{
	// echo "Cart empty";
	alertMassageAndRedirect('cart is empty !','product.php');
	exit();
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

  <form method="post" action="update_cart.php" >
    <div class="site-section">
      	<div class="container">
        <div class="col-md-12">
          <label class="text-black h4" for="coupon">ตะกร้าสินค้า</label>
        </div>
        <div class="row mb-5">
          <div class="col-md-12" >
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="product-thumbnail">รูป</th>
                    <th class="product-name">สินค้า</th>
										<th class="product-name">จำนวนคงเหลือ</th>
                    <th class="product-price">ราคา</th>
                    <th class="product-quantity">จำนวน</th>
                    <th class="product-total">รวม</th>
                    <th class="product-remove">ลบ</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                $Total = 0;
                $SumTotal = 0;

                for($i=0;$i<=(int)$_SESSION["intLine"];$i++)
                {
                  if($_SESSION["strProductID"][$i] != "")
                  {
                    $strProductID = $_SESSION["strProductID"][$i] ;
                    $objResult = $conn->select('products', array('productid' => $strProductID), true);

                  $Total = $_SESSION["strQty"][$i] * $objResult["productsprice"];
                  $SumTotal = $SumTotal + $Total;
                  ?>

                  <tr>
                    <td class="product-thumbnail">
                      <img src="images/<?php echo $objResult["productsphoto"]; ?>" alt="Image" class="img-fluid">
                    </td>
                    <td class="product-name">
                      <h2 class="h5 text-black"><?php echo $objResult["productsname"];?></h2>
                    </td>
										  <td><?php echo $objResult["amount"]; ?></td>
                    <td>฿<?php echo $objResult["productsprice"]; ?></td>
                    <td>
                      <div class="input-group mb-3" style="max-width: 120px;">
                        <div class="input-group-prepend">
                          <button class="btn btn-outline-black js-btn-minus" style=" background-color: #AAA8A8; color: white;" type="button">&minus;</button>
                        </div>
                        <input type="text"  autocomplete="off"  class="form-control text-center" name="txtQty<?php echo $i;?>" value="<?php echo $_SESSION["strQty"][$i]; ?>" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1" maxlength="3">
                        <div class="input-group-append">
                          <button class="btn btn-outline-black js-btn-plus" style=" background-color: #AAA8A8; color: white;" type="button">&plus;</button>
                        </div>
                      </div>

                    </td>
                    <td>฿<?php echo $Total; ?></td>
                    <td><a href="delete_car.php?Line=<?php echo $i;?>" class="btn btn-primary height-auto btn-sm" style=" background-color: #000000; color: white; border-color: black;">X</a></td>
                  </tr>

                  <?php
                              	  }
                                }
                                ?>

                </tbody>
              </table>
            </div>
					</div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="row mb-5">
              <div class="col-md-6 mb-3 mb-md-0">
                <button class="btn btn-black btn-sm btn-block" style=" background-color: #000000; color: white; border-color: black;" >คำนวณราคาสินค้าใหม่</button>
              </div>
              <div class="col-md-6">
                <button class="btn btn-outline-black btn-sm btn-block" style=" background-color: #FFFFFF; border-color: black; color: black;" name="__cmd" value="product">เลือกสินค้าเพิ่ม</button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <!-- <label class="text-black h4" for="coupon">Coupon</label> -->
                <!-- <p>Enter your coupon code if you have one.</p> -->
              </div>
              <div class="col-md-8 mb-3 mb-md-0">
                <!-- <input type="text"  autocomplete="off"  class="form-control py-3" id="coupon" placeholder="Coupon Code"> -->
              </div>
              <div class="col-md-4">
                <!-- <button class="btn btn-primary btn-sm px-4">Apply Coupon</button> -->
              </div>
            </div>
          </div>
          <div class="col-md-6 pl-5">
            <div class="row justify-content-end">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-12 text-right border-bottom mb-5">
                    <h3 class="text-black h4 text-uppercase">ยอดรวม</h3>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-6">
                    <span class="text-black">รวมทั้งหมด</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black"><?php echo number_format($SumTotal,2);?></strong>
                  </div>
                </div>
                <!-- <div class="row mb-5">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">$230.00</strong>
                  </div>
                </div> -->

                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-primary btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="__cmd" value="save_product">สั่งซื้อสินค้า</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

	    </div>
		</div>
	</form>


    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

	<script type="text/javascript">

	</script>
  </body>
</html>
