<?php
session_start();
require_once "ConnectDatabase/connectionDb.inc.php";

//ข่าวสารกิจกรรม
$sql = "SELECT *  from news ";
$select_allNews = $conn->queryRaw($sql);
$totalNews = sizeof($select_allNews);

$sql = "SELECT * FROM products ORDER BY RAND() LIMIT 10";
$tbl_products = $conn->queryRaw($sql);


$sqlcounter = "SELECT * FROM counter";
$tbl_counter = $conn->queryRaw($sqlcounter);

$_SESSION["count"] = "0";

if($tbl_counter[0]["count"] == "0" || $tbl_counter[0]["count"] == "" || $tbl_counter[0]["count"] == null) {
  $value = array(
    "count" => 1
  );
  if ($conn->create("counter", $value)) {
  }
} else {
  $count = intval($tbl_counter[0]["count"]) + 1;
  $_SESSION["count"] = $count;
  $valueUpdate = array(
    "count" => $count
  );

  if ($conn->update("counter", $valueUpdate, array("id" => $tbl_counter[0]["id"]))) {
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">
</head>

<body>

  <div class="site-wrap">


    <?php include "Menu/navbar.php" ?>

    <div class="site-blocks-cover" data-aos="fade" style="background-color:#EBEBEB;  ">
      <div class="container">
        <div class="row">
          <div class="col-md-6 ml-auto order-md-2 align-self-start">
            <div class="site-block-cover-content">
              <h1>ธาดาเซรามิก</h1>
              <h2 class="sub-title">#Hand Made Ceramic</h2>
              <!-- <p><a href="#" class="btn btn-black rounded-0">Shop Now</a></p> -->
            </div>
          </div>
          <div class="col-md-6 order-1 align-self-center">
            <img src="images/bg4.jpg" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </div>


    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="title-section mb-5 col-12">
            <h2 class="text-uppercase">สินค้า</h2>
            <hr>
          </div>
        </div>
        <div class="row">
          <?php
          $index = 0;
          foreach ($tbl_products as $row) {
            $index++;

          ?>
            <div class="col-lg-4 col-md-6 item-entry mb-4">
              <a href="product-detail.php?id=<?php echo $row['productid']; ?>" class="product-item md-height bg-gray d-block">
                <img src="images/<?php echo $row['productsphoto']; ?>" alt="Image" class="img-fluid">
              </a>
              <h2 class="item-title"><a href="news-detail.php?id=<?php echo $row['productid']; ?>"><?php echo $row['topic']; ?></a></h2>
              <strong class="item-price"><?php echo substr($row['productsname'], 0, 300) ?>... <a href="product-detail.php?id=<?php echo $row['newsid']; ?>">อ่านต่อ</a></strong>
            </div>

          <?php
          }
          ?>

        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="title-section mb-5 col-12">
            <h2 class="text-uppercase">ข่าวสาร</h2>
            <hr>
          </div>
        </div>
        <div class="row">
          <?php
          $index = 0;
          foreach ($select_allNews as $row) {
            $index++;

          ?>
            <div class="col-lg-4 col-md-6 item-entry mb-4">
              <a href="news-detail.php?id=<?php echo $row['newsid']; ?>" class="product-item md-height bg-gray d-block">
                <img src="images/<?php echo $row['photo']; ?>" alt="Image" class="img-fluid">
              </a>
              <h2 class="item-title"><a href="news-detail.php?id=<?php echo $row['newsid']; ?>"><?php echo $row['topic']; ?></a></h2>
              <strong class="item-price"><?php echo substr($row['message'], 0, 300) ?>... <a href="news-detail.php?id=<?php echo $row['newsid']; ?>">อ่านต่อ</a></strong>
            </div>

          <?php
          }
          ?>

        </div>
      </div>
    </div>


    <!-- <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="title-section text-center mb-5 col-12">
            <h2 class="text-uppercase">Most Rated</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 block-3">
            <div class="nonloop-block-3 owl-carousel">
              <div class="item">
                <div class="item-entry">
                  <a href="#" class="product-item md-height bg-gray d-block">
                    <img src="images/model_1.png" alt="Image" class="img-fluid">
                  </a>
                  <h2 class="item-title"><a href="#">Smooth Cloth</a></h2>
                  <strong class="item-price"><del>$46.00</del> $28.00</strong>
                  <div class="star-rating">
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-entry">
                  <a href="#" class="product-item md-height bg-gray d-block">
                    <img src="images/prod_3.png" alt="Image" class="img-fluid">
                  </a>
                  <h2 class="item-title"><a href="#">Blue Shoe High Heels</a></h2>
                  <strong class="item-price"><del>$46.00</del> $28.00</strong>

                  <div class="star-rating">
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-entry">
                  <a href="#" class="product-item md-height bg-gray d-block">
                    <img src="images/model_5.png" alt="Image" class="img-fluid">
                  </a>
                  <h2 class="item-title"><a href="#">Denim Jacket</a></h2>
                  <strong class="item-price"><del>$46.00</del> $28.00</strong>

                  <div class="star-rating">
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                  </div>

                </div>
              </div>
              <div class="item">
                <div class="item-entry">
                  <a href="#" class="product-item md-height bg-gray d-block">
                    <img src="images/prod_1.png" alt="Image" class="img-fluid">
                  </a>
                  <h2 class="item-title"><a href="#">Leather Green Bag</a></h2>
                  <strong class="item-price"><del>$46.00</del> $28.00</strong>
                  <div class="star-rating">
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="item-entry">
                  <a href="#" class="product-item md-height bg-gray d-block">
                    <img src="images/model_7.png" alt="Image" class="img-fluid">
                  </a>
                  <h2 class="item-title"><a href="#">Yellow Jacket</a></h2>
                  <strong class="item-price">$58.00</strong>
                  <div class="star-rating">
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                    <span class="icon-star2 text-warning"></span>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
 -->

    <!-- <div class="site-blocks-cover inner-page py-5" data-aos="fade">
      <div class="container">
        <div class="row">
          <div class="col-md-6 ml-auto order-md-2 align-self-start">
            <div class="site-block-cover-content">
            <h2 class="sub-title">#New Summer Collection 2019</h2>
            <h1>New Shoes</h1>
            <p><a href="#" class="btn btn-black rounded-0">Shop Now</a></p>
            </div>
          </div>
          <div class="col-md-6 order-1 align-self-end">
            <img src="images/model_6.png" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </div> -->

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <footer class="site-footer custom-border-top">
      <?php include "Menu/footer.php" ?>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

</body>

</html>