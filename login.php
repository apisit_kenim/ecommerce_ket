<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";

if ($_REQUEST[btnLogin] === 'เข้าสู่ระบบ') {

  $user_name = getIsset('username');
  $password = getIsset('password');

  $chk_login = $conn->select('member', array('username' => $user_name, 'password' => $password), true);

  if ($chk_login != null) {

    if ($chk_login["isActive"] == true || $chk_login["isActive"] == "1") {
      $_SESSION["isLogin"] = true;

      $_SESSION["memberid"] = $chk_login["memberid"];
      $_SESSION["memberName"] = $chk_login["name"];
      $_SESSION["memberAddress"] = $chk_login["address"] . ' ต.' . $chk_login["district"] . ' อ.' . $chk_login["amphoe"] . ' จ.' . $chk_login["province"];
      $_SESSION["memberZipcode"] = $chk_login["zipcode"];
      $_SESSION["memberPhone"] = $chk_login["phone"];
      $_SESSION["isLogin"] = true;

      session_write_close();

      // redirectTo($_SERVER["SERVER_PORT"] . '/index.php');
      header('Location: ./index.php');
    } else {
      alertMassage("Account Is Locked Please Contact Shop.");
    }
  } else {
    // $message = generate_action_tag('error', 'Username หรือ Password ผิดพลาด กรุณาตรวจสอบ');
    alertMassage("Invalid User Account !");
  }
}
// }else if ($_REQUEST[btnForgot]){
// 		header('Location: ./index.php');
// }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>&mdash; ห้างหุ้นส่วนจำกัด ธาดาเซรามิก &mdash; </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
  <link rel="stylesheet" href="fonts/icomoon/style.css">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">


  <link rel="stylesheet" href="css/aos.css">

  <link rel="stylesheet" href="css/style.css">

</head>

<body>

  <div class="site-wrap">

    <?php include "Menu/navbar.php" ?>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="h3 mb-12 text-black">เข้าสู่ระบบ</h2>
          </div>
          <br /> <br /> <br />
          <div class="col-md-12">

            <form method="post">
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="c_subject" class="text-black">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                  <input type="text" autocomplete="off" class="form-control" id="c_subject" name="username" required="required" maxlength="10">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <label for="c_subject" class="text-black">รหัสผ่าน <span class="text-danger">*</span></label>
                  <input type="password" class="form-control" id="c_subject" name="password" required="required" maxlength="10">
                </div>
              </div>


              <div class="form-group row">
                <div class="col-lg-12">
                  <input type="submit" class="btn btn-primary btn-lg btn-block" style=" background-color: #000000; color: white; border-color: black;" name="btnLogin" value="เข้าสู่ระบบ">
                  <a href="forgot.php" style="color: gray;">ลืมรหัสผ่าน?</a>
                </div>
              </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="bg-light py-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mb-0"></div>
      </div>
    </div>
  </div>

  <footer class="site-footer custom-border-top">
    <?php include "Menu/footer.php" ?>
  </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>

  <script>
    $(document).on('keypress', '.txtNumber ', function(event) {
      console.log(event.charCode);
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
      }
      return true;
    });
  </script>

</body>

</html>